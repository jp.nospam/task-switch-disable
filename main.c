#include <stdio.h>
#include <windows.h>

HHOOK hHook;

LRESULT CALLBACK LowLevelKeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
        KBDLLHOOKSTRUCT *kbhs = (KBDLLHOOKSTRUCT *) lParam;
        if (nCode == HC_ACTION)
                if (kbhs->vkCode == VK_TAB && kbhs->flags & LLKHF_ALTDOWN)
                        return 1;
        return CallNextHookEx(hHook, nCode, wParam, lParam);
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, 
                        LPSTR lpCmdLine, int nCmdShow)
{
        printf("Restore ALT+TAB by exiting the program.");
        hHook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, 
                                        hInstance, 0);
        MSG msg;
        while(GetMessage(&msg, NULL, 0, 0))
        {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
         }
         return msg.wParam;
}