# Task Switch Disable

Disables ALT+TAB while running. 

Designed for FPS gaming to not accidently ALT+TAB out.

**Run as admin.**
